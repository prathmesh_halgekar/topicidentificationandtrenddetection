import nltk

from dataReader import get_tokenized_tfidf
from evaluation import evaluate
from topic_identification_step1 import find_sentence_from_doc
from topic_identification_step1 import get_synset_token

dict_tokenized_tfidf = {}

dataset_path = "C:/Users/Prathmesh/Downloads/dataset2"


def process1():
    doc_topic_words_process1 = {}
    eval_doc_topics = {}
    dict_tokenized_tfidf, doc_topic_words_nmf, token_list_for_bigrams = get_tokenized_tfidf(dataset_path)
    bigrams = nltk.bigrams(token_list_for_bigrams)
    cfd = nltk.ConditionalFreqDist(bigrams)

    for name, value_tuple in dict_tokenized_tfidf.items():
        topic_word_list = []
        eval_topic_list = []
        print("For document : ", name)
        print("  ----------------------  ")
        token_score_list = value_tuple[0]
        sentence_list = value_tuple[1]
        for word, score in token_score_list:
            topic_word_list.append(word)
            eval_topic_list.append(generate_model(cfd, word.lower(), 2))
            target_sentence_list = find_sentence_from_doc(word, sentence_list)
            for synset, score in get_synset_token(word, target_sentence_list).items():
                print("")
                print("For word : ", word)
                print("Best Synset : ", synset.name(), " Hypernym : ", synset.hypernyms(), " with score : ", score)
                print("Probable topic : ", generate_model(cfd, word, 2))

                # print("\n")
        print("")
        print("  ----------------------  ")
        eval_doc_topics[name] = eval_topic_list
        doc_topic_words_process1[name] = topic_word_list

    doc_topic_words_nmf_eval = {}
    for name, topic_word_list in doc_topic_words_nmf.items():
        topic_list = []
        for word in topic_word_list:
            topic_list.append(generate_model(cfd, word.lower(), 2))
            doc_topic_words_nmf_eval[name] = topic_list

    evaluation = {}
    for name, topic_word_list in doc_topic_words_process1.items():
        topic_list = []
        score, common_word_list = calculateSimilarWords(topic_word_list, doc_topic_words_nmf[name])
        for word in common_word_list:
            topic_list.append(generate_model(cfd, word.lower(), 2))
        evaluation[name] = topic_list
        print("For Doc : ", name, "Common Word List : ", common_word_list, " With Score : ", score)

    print("")
    print("Evaluation with wordnet technique")
    evaluate(eval_doc_topics)
    print("-------------------------------------")
    print("Evaluation with nmf technique")
    evaluate(doc_topic_words_nmf_eval)
    print("-------------------------------------")
    print("Evaluation using combination technique")
    evaluate(evaluation)
    print("-------------------------------------")


def calculateSimilarWords(list1, list2):
    common_word_list = list(set(list1).intersection(set(list2)))
    score = len(common_word_list) / len(list1)
    return score, common_word_list


def generate_model(cfdist, word, num=2):
    topic = ""
    is_first = True
    for i in range(num):
        # print(word, end=" ")
        if is_first:
            topic = topic + "" + word
            is_first = False
        else:
            topic = topic + " " + word
        if len(cfdist[word]) != 0:
            if word == cfdist[word].max() or cfdist[word].max() == '' or cfdist[
                word].max() == ' ':  # we don't want duplicate words
                break
            else:
                word = cfdist[word].max()
        else:
            break
    return topic


# App main function
if __name__ == '__main__':
    process1()
