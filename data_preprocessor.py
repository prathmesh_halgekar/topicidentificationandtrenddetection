# for impording pdf in python
import math
import re
import string

from nltk import pos_tag
# we will remove the punctuations, stopwords and normalize the corpus.
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer

from nmf.nmf_anyfile import process_nmf

regex = re.compile('[%s]' % re.escape(string.punctuation))

# Preprocessing block 1
# Tokenizing, filtering the stop words and punctuation symbols.


stopWords = set(stopwords.words('english'))

stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()


def clean(doc):  # defining function
    # Preprocessing block 2
    # Filtering the stop words and punctuation symbols - Redundant.
    # Lemmatizing
    stop_free = " ".join(i for i in doc.split() if i not in stop)
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    # lemmatization disabled currently as it causes complications later while searching
    # for sentences with lemmatized 'keyword'. For e.g. 'units' -> 'unit'
    return normalized


def tf(word, blob):
    return blob.count(word) / len(blob)


def n_containing(word, bloblist):
    result = 0
    for name, value_tuple in bloblist.items():
        token_list = value_tuple[0]
        if word in token_list:
            result = result + 1
    return result


def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))


def getPositiveScore(a):
    return (abs(a) + a) / 2


def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)


is_noun = lambda pos: pos[:2] == 'NN'


def get_tfidf_for_document(global_data, token_list_for_bigrams):
    #  Considering for 'n' input document.
    #  return a dictionary of - > key = doc_name, value - > (top 'n' tf*idf scored words , sentences)
    # docList = []
    result = {}
    return_doc_processed = {}
    doc_clean = []
    for name, value_tuple in global_data.items():
        doc_clean = []
        token_list = value_tuple[0]
        for token, pos in pos_tag(token_list):
            if is_noun(pos):
                doc_clean.append(clean(token))
        global_data[name] = (doc_clean, value_tuple[1])

    token_list_for_bigrams = [clean(word).lower() for word in token_list_for_bigrams]

    bloblist = global_data
    return_list_nmf = {}
    for name, value_tuple in bloblist.items():
        token_list = value_tuple[0]
        scores = {word: tfidf(word, token_list, bloblist) for word in token_list}
        scores_list = [(word, tfidf(word, token_list, bloblist)) for word in token_list]
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        print("\tFor Doc: {}".format(name))
        for word, score in sorted_words[:10]:
            result[word] = score
            print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))
        return_doc_processed[name] = (sorted_words[:10], value_tuple[1])

    token_list_nmf = []
    token_list_with_duplicates_nmf = []
    for name, value_tuple in bloblist.items():
        token_list = value_tuple[0]
        token_list_nmf += token_list
    token_list_with_duplicates_nmf = token_list_nmf
    token_list_nmf = list(dict.fromkeys(token_list_nmf))  # reomving the duplicates

    for name, value_tuple in bloblist.items():
        token_score_list_nmf = []
        token_list = value_tuple[0]
        for token in token_list_nmf:  # tokens in all the document
            if token in token_list:  # tokens in this(iterated) document
                token_score_list_nmf.append((token, token_list_with_duplicates_nmf.count(
                    token)))  # (token , count), tokens present in this document
            else:
                token_score_list_nmf.append((token, 0))  # This is needed to make even length size list for nmf!

        return_list_nmf[name] = token_score_list_nmf

    # Calling the nmf process
    print(" calling process nmf from data_preprocessor ")
    doc_topic_words_nmf = process_nmf(return_list_nmf)
    return return_doc_processed, doc_topic_words_nmf, token_list_for_bigrams


# App main function
if __name__ == '__main__':
    get_tfidf_for_document()
