doc_keywords = {}
doc_keywords["2009_1"] = ["trend detection", "topic identification", "citation mining", "text mining"]
doc_keywords["2009_2"] = ["ThemeRiver", "visualization metaphors", "trend analysis", "timeline"]
doc_keywords["2010_1"] = ["graph analysis", "federated querying", "data mining", "semantic web", "drug interactions"]
doc_keywords["2010_2"] = ["Search based software engineering", "software engineering", "length", "branch coverage",
                          "code coverage", "genetic algorithm", "infeasible goal", "collateral coverage"]
doc_keywords["2011_1"] = ["anisotropy coefficient", "roughness spacing parameters", "surface texture",
                          "texture aspect ratio"]
doc_keywords["2011_2"] = ["planetary gear", "reduction gear", "vertical rotation axis", "wind turbine"]
doc_keywords["2012_1"] = ["gender differences", "mathematics", "gender equity", "international data sets"]
doc_keywords["2012_2"] = ["educational acceleration", "gifted", "longitudinal study", "STEM", "talent searches"]
doc_keywords["2013_1"] = ["research assessment", "peer review", "metrics", "excellence"]
doc_keywords["2013_2"] = ["deep architecture", "unsupervised training", "deep belief network", "belief network",
                          "stacked denoising auto-encoder", "non-convex optimization"]
doc_keywords["2014_1"] = ["Visually impaired", "blind", "accessibility", "design", "ideation", "multicultural", "AI",
                          "artificial intelligence"]
doc_keywords["2014_2"] = ["Artificial Intelligence", "Knowledge", "Law Enforcement", "Fraud"]
doc_keywords["2015_1"] = ["Markov chain", "Monte Carlo", "MCMC", "sampling", "stochastic algorithms"]
doc_keywords["2015_2"] = ["deep architectures", "unsupervised pre-training", "belief network",
                          "stacked denoising auto-encoders", "non-convex optimization"]


def evaluate(doc_topic_words):
    tp = 0
    fp = 0
    fn = 0
    for name, topic_keywords in doc_topic_words.items():
        actual_topics = [word.lower() for word in doc_keywords[name]]  # gold standard
        for index, computed_word in enumerate(topic_keywords):
            if computed_word in actual_topics:
                tp = tp + 1
                break
            if index == (len(topic_keywords) - 1):
                fp = fp + 1
    print("total True Positive : ", tp)
    print("total False Positive : ", fp)

    for name, topic_keywords in doc_keywords.items():
        computed_topics = doc_topic_words[name]
        for index, topic_word in enumerate(topic_keywords):  # gold standard
            if topic_word.lower() not in computed_topics:
                str()  # do nothing
            else:
                break
            if index == (len(topic_keywords) - 1):
                fn = fn + 1

    print("total False Negative : ", fn)

    print("Precision : ", (tp / (tp + fp)))
    print("Recall : ", (tp / (tp + fn)))


# App main function
if __name__ == '__main__':
    test_doc_keywords = {}
    test_doc_keywords["2009_1"] = ["trend detection1", "topic identification1", "citation mining2", "text mining1",
                                   "citation mining3"]
    test_doc_keywords["2009_2"] = ["visualization metaphors", "trend analysis", "timeline"]
    evaluate(test_doc_keywords)
    # evaluate()
