import csv
import math

import matplotlib.pyplot as plt
import numpy as np
from nltk.stem.wordnet import WordNetLemmatizer
from sklearn.decomposition import NMF

lemma = WordNetLemmatizer()


def clean(doc):  # defining function
    # Preprocessing block 2
    # Filtering the stop words and punctuation symbols - Redundant.
    # Lemmatizing
    stop_free = " ".join(i for i in doc.lower().split() if i not in stop)
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

    # recommended to read https://stevenloria.com/tf-idf/
    # to understand below code.


def tf(word_count, blob):
    return word_count / len(blob)


def n_containing(target_word, bloblist):
    result = 1;
    for doc_name, words_n_count in bloblist.items():
        for index, (word, count) in enumerate(words_n_count):
            if target_word in word and count > 0:
                result = result + 1
    return result


def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))


def tfidf(word_count, word, blob, bloblist):
    return tf(word_count, blob) * idf(word, bloblist)


def getPositiveScore(a):
    return (abs(a) + a) / 2


dataset = {}


def readMyFile(filename):
    scores = []
    is_first_time = True
    with open(filename, 'rt') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            if is_first_time:
                dates = row
                is_first_time = False
            else:
                scores.append(row)
        j = 1
        tempList = []
        for date in dates:
            if date != '':
                tempList = []
                for i in range(0, len(scores)):
                    tempList.append((scores[i][0], int(scores[i][j])))
                j = j + 1
                dataset[date] = tempList
    return dates, scores


def process_nmf(dataset):
    print(" process nmf started ")
    bloblist = dataset
    scores = {}
    matrix = []
    total_words = []
    years = []
    num_of_docs = 0
    doc_name_lists = []
    for doc_name, words_n_count in bloblist.items():
        years.append(doc_name.split("_")[0])
        doc_name_lists.append(doc_name)
        num_of_docs = num_of_docs + 1
        scores = []
        for index, (word, count) in enumerate(words_n_count):
            scores.append(getPositiveScore(round(tfidf(count, word, words_n_count, bloblist), 2)))
            total_words.append(word)
        matrix.append(scores)
    years = list(dict.fromkeys(years))
    A = np.array(matrix)
    print(A.shape)

    error_values = {}
    for a in range(1, 10):  # loop for checking on various n_values
        error = np.zeros((A.shape))  # initialize zero matrix of A.shape()
        square_error = np.zeros((A.shape))
        model = NMF(n_components=a, init='random', random_state=0)
        W = model.fit_transform(A)
        H = model.components_
        result = np.zeros((A.shape))
        # iterate through rows of X
        for i in range(len(W)):
            # iterate through columns of Y
            for j in range(len(H[0])):
                # iterate through rows of Y
                for k in range(len(H)):
                    result[i][j] += W[i][k] * H[k][j]
        error = np.subtract(A, result)  # error for every n_component value
        # print(error)
        square_error = (error * error)  # square error for every n_component value
        # print(square_error)
        sum = np.sum(square_error)  # cumulative of every element of square error
        # print(sum)
        error_values[a] = math.sqrt(sum)
    sorted_words = sorted(error_values.items(), key=lambda x: x[1], reverse=False)
    print("Min least error , sorted_words[:1] = ",
          sorted_words[:1])  # cumulative square error for every n_component value

    error = np.zeros(A.shape)
    square_error = np.zeros(A.shape)
    model = NMF(n_components=sorted_words[0][0], init='random',
                random_state=0)  # take n_component value of minimum error
    W = model.fit_transform(A)
    H = model.components_

    result = np.zeros(A.shape)
    # iterate through rows of X
    for i in range(len(W)):
        # iterate through columns of Y
        for j in range(len(H[0])):
            # iterate through rows of Y
            for k in range(len(H)):
                result[i][j] += W[i][k] * H[k][j]
    error = np.subtract(A, result)
    square_error = (error * error)
    # print(error)
    sum = np.sum(square_error)

    topics = []
    j = 1
    topWords = []
    for array in H:
        m = (-array).argsort()[:10]
        topWords.append(m)
        print(m)  # displays position of top 10 words from every topic

    topic_list = []
    for index, array in enumerate(topWords):
        topic_word = []
        print('Topic ', index)
        print('--------------')
        for i in range(len(array)):
            topic_word.append(total_words[array[i]])
            print(total_words[array[i]])  # displays top 10 words from every topic
        print('--------------')
        topic_list.append(topic_word)

    doc_topic_words = {}
    topics = []
    j = 0
    for array in W:
        m = max(array)
        print(m)

        for i in range(0, len(array)):
            if (array[i] == m):
                print("the document", doc_name_lists[j], "belongs to topic",
                      i)  # shows document with its respective topic
                doc_topic_words[doc_name_lists[j]] = topic_list[i]
                topics.append(i)
                j += 1
                break
    print(topics)  # displays topics in an order of their belonging in document

    documents = list(range(1, num_of_docs + 1))

    width = 1 / 1.5

    plt.bar(documents, topics, width, color="pink")
    # naming the x axis
    plt.xlabel('Documents')
    # naming the y axis
    plt.ylabel('Topics')

    # giving a title to my graph
    plt.title('Between documents and topics')
    # function to show the plot
    plt.show()

    # for trend detection
    trend = []
    num_topics = sorted_words[0][0]  # n_component+1
    total_doc = len(topics)

    # for j in range(1, num_topics):  # no of topics
    #     l = 0
    #     topics1Arr = np.zeros(5)  # documents are divided in 5 years
    #     for i in range(1, total_doc):
    #         if topics[i - 1] == (j - 1):
    #             topics1Arr[l] = topics1Arr[l] + 1
    #         if (i % 5 == 0):
    #             l = l + 1
    #     trend.append(topics1Arr)
    # # print(topics1Arr)
    # print(trend)

    # for j in range(1, num_topics):  # no of topics
    #     l = 0
    #     topics1Arr = np.zeros(len(years))  # documents are divided in 5 years
    #     for i in range(1, total_doc):
    #         if topics[i - 1] == (j - 1):
    #             topics1Arr[l] = topics1Arr[l] + 1
    #         if (i % (len(years)) == 0):
    #             l = l + 1
    #     trend.append(topics1Arr)
    # print(trend)

    count = 1
    noDocs = [2, 2, 2, 2, 2, 2, 2]
    for j in range(0, num_topics):  # loop upto num of topics
        count = 0
        topicsArr = np.zeros(7)  # initial arraysize
        for k in range(0, len(years)):
            for l in range(0, noDocs[k]):
                # print(topics1[count])
                if (topics[count] == j):
                    topicsArr[k] = topicsArr[k] + 1
                count = count + 1
        trend.append(topicsArr)

    # naming the x axis
    plt.xlabel('Year')
    # naming the y axis
    plt.ylabel('No.of documents from the topic')

    # giving a title to my graph
    plt.title('Trend for topic 0')
    plt.plot(years, trend[0], linestyle='--', marker='o', color="red")
    plt.show()

    # for i in range(0, len(years) + 1):
    #     if i <= (len(years) - 5):
    #         # naming the x axis
    #         plt.xlabel('Year')
    #         # naming the y axis
    #         plt.ylabel('No.of documents from the topic')
    #
    #         # giving a title to my graph
    #         plt.title('Trend for topic 0')
    #         plt.plot(years[i:(i + 5)], trend[0], linestyle='--', marker='o', color="red")
    #         plt.show()

    return doc_topic_words
    print(" process nmf ended ")

# App main function
# if __name__ == '__main__':
#     process_nmf()  # pass correct parameters to this function.
