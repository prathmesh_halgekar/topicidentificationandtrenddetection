# README #

This README would normally document whatever steps are necessary to get your application up and running.

Steps to run the project :
----------------------
* Give the dataset path from your system in 'topic_identification_step2.py' line 10.
* run 'topic_identification_step2.py'.

### What is this repository for? ###
Topic Identification :
----------------------
1) First, we use the tf*idf measure to extract keywords from document. Each keyword is then mapped into topics in the concept hierarchy T.
*Current Status* : We have been successful to extract keyword using tf*idf.
Next Step : We have to pick 'n' highest(tf*idf) terms and map it to 'Concept hierarchy'. 
*Issue* : For 'Concept hierarchy' T we can use Wordnet. But how do we find out the actual sense of a word? For e.g word -> 'bank'
Possible solution : for term in terms
						synSetList = findSynSetUsingWordNet('bank') // https://www.youtube.com/watch?v=T68P5-8tM-Y
						for synSet in synSetList
							listOfExampleSentence = synSet.examples() // https://www.youtube.com/watch?v=T68P5-8tM-Y
							compareSimilarity(targetSentenceInWhichTermAppearsFromOurDocument, listOfExampleSentence) //ref:  http://nlpforhackers.io/wordnet-sentence-similarity/
						// pick the sentence from 'listOfExampleSentence' according to the result of above comparison
						// add the corresponding 'word' from the above sentence to list of Identified topic.

Note* both the targetSentenceInWhichTermAppears and listOfExampleSentence needs to be POS tagged as shown in http://nlpforhackers.io/wordnet-sentence-similarity/		

Also for the final Topic detection for the document we need to refer to [1]
[1] Minh-Hoang Le, Tu-Bao Ho, Yoshiteru Nakamori, Detecting Emerging Trends from Scientific
Corpora, Inter- national Journal of Knowledge and Systems Sciences, 2005. Sec 2.2

I think the overall approach is similar to http://www.dh2012.uni-hamburg.de/conference/programme/abstracts/automatic-topic-hierarchy-generation-using-wordnet.1.html


If we achieve the above part then we complete 'Topic Detection' part of the project. After that is 'Trend detection'.			
 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact