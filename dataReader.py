from pathlib import Path

import nltk.data
from nltk.tokenize import word_tokenize

from data_preprocessor import clean
from data_preprocessor import get_tfidf_for_document

global_sentence_list = []  # To store all sentences of a document
global_token_list = []  # To store all tokens of a document

global_data = {}
import os


def get_tokenized_tfidf(dataset_path):
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    file_paths = []
    token_list_for_bigrams = []
    for filename in os.listdir(dataset_path):
        file_paths.append(dataset_path + "/" + filename)

    for index, file_path in enumerate(file_paths):
        fp = open(file_path)
        file_name = Path(file_path).resolve().stem
        data = fp.read()
        token_list = word_tokenize(data)
        token_list_for_bigrams = token_list_for_bigrams + [clean(word) for word in token_list]
        sentence_list = tokenizer.tokenize(data)
        global_data[file_name] = (token_list, sentence_list)
    return get_tfidf_for_document(global_data, token_list_for_bigrams)


def get_global_sentence_list(file_path):
    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    fp = open(file_path)
    data = fp.read()
    global_token_list = word_tokenize(data)
    global_sentence_list = tokenizer.tokenize(data)
    return global_sentence_list


if __name__ == '__main__':
    # testSome("model")
    path = ["/Users/Prathmesh/Downloads/test.txt", "/Users/Prathmesh/Downloads/test2.txt"]
    get_tokenized_tfidf(path)
