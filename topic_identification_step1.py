from nltk.corpus import wordnet
from nltk.tokenize import word_tokenize

from SentenceComparision import sentence_similarity
from data_preprocessor import clean


def find_sentence_from_doc(target_word, sentence_list):
    # Returning All matching sentence from the document
    result_list = []
    processed_sentence_list = []
    # process this sentence_list
    for sentence in sentence_list:
        processed_sentence_list.append(clean(sentence))

    for i, sentence in enumerate(processed_sentence_list):
        for token in word_tokenize(sentence):
            if target_word == token:
                result_list.append(sentence_list[i])
    return result_list


def get_synsets(token):
    return wordnet.synsets(token)  # Assuming that 'bank is the word from tf*idf list


def get_synset_token(token, target_sentence_list):
    global_dictionary = {}
    result = {}
    synsets = get_synsets(token)
    focus_sentence = target_sentence_list
    for target_sentence in target_sentence_list:
        focus_sentence = target_sentence
        for syset in synsets:
            score = 0
            exampleList = syset.examples()
            for sentence in exampleList:
                score += sentence_similarity(sentence, focus_sentence, token)
            global_dictionary[syset] = score / len(target_sentence_list)

    sorted_x = sorted(global_dictionary.items(), key=lambda t: t[1], reverse=True)
    for synset, score in sorted_x[:1]:
        result[synset] = score
    return result


# App main function
if __name__ == '__main__':
    get_synset_token()  # pass correct parameters to this function.
